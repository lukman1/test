package snippet;

public class Snippet {
	server:
	  port: 8080
	  session:
	    cookie:
	      domain: springboot.com
	spring:
	  app:
	    domain: springboot.com
	    protocol: http
	    baseUrl: ${spring.app.protocol}://${spring.app.domain}:${server.port}
	    cdn-url: 
	  application:
	    name: springbootjpa
	    home: C:\Users\Lukman.DESKTOP-2ON3R5B\Documents\Spring Learn\springbootjpa
	  messages:
	    encoding: UTF-8
	#Database / Hibernate / JPA
	  datasource:
	    url: jdbc:mysql://localhost:3306/springboot?useSSL=false&useUnicode=true&characterEncoding=UTF-8
	    username: root
	    password: 
	    dialect: org.hibernate.dialect.MySQL5InnoDBDialect
	    driver: com.mysql.jdbc.Driver
	    show-sql: true
	  jpa:
	    hibernate:
	      ddl-auto: create
	      
	    naming:
	    physical-strategy: org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
	    ddl-auto: create
	    show-sql: true
}

