package com.shopping.shop.Model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper =false)
@Table(name="shopping")
public class Shopping {
	
@Id
@GeneratedValue(strategy =GenerationType.AUTO)
private Long id;
	
@Column(name="name")
private String name;

@CreatedDate
@Column(name = "CREATED_DATE")
private Date createdDate;


}
